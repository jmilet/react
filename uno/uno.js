var Texto = React.createClass({
    render: function() {
      return (<div>Clicked {this.props.times} | {this.props.other_state}</div>);
    }
});

var Button = React.createClass({
    render: function() {
        return (<button onClick={this.props.handler}>Click me</button>);
    }
});

var Content = React.createClass({
    getInitialState: function() {
        return {times: 0, ticks: 0, other_state: 100};
    },
    componentDidMount: function() {
        this.timer = setInterval(function() { this.setState({ticks: this.state.ticks + 1}) }.bind(this), 1000);
    },
    componentWillUnmount: function() {
        clearInterval(this.timer);
    },
    click: function(event) {
        this.setState({times: this.state.times + 1});
        $.get("https://api.github.com/users/octocat/gists", function(result) {
            this.setState({other_state: result[this.state.times - 1].url});
        }.bind(this));
    },
    render: function() {
        return (
            <div>
                <Button handler={this.click} />
                <br />
                <Texto times={this.state.times} other_state={this.state.other_state} />
                <br />
                <Texto times={this.state.ticks} />
            </div>
        );
    }
});

ReactDOM.render(
  <Content />,
  document.getElementById('main')
);

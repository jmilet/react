var Counter = React.createClass({
    getInitialState: function() {
        var websocket = new WebSocket("ws://radiacrom.com:8000", "echo-protocol");

        websocket.onopen = function(event) {
            websocket.send("hola");
        }.bind(this);

        websocket.onmessage = function(event) {
            // console.log(JSON.parse(event.data));
            this.setState({data: JSON.parse(event.data)});
            // this.setState({data: event.data})
        }.bind(this);

        return {
            websocket: websocket,
            data: {}
        };
    },
    componentDidMount: function() {
    },
    componentWillUnmount: function() {
        this.state.websocket.close();
    },
    render: function() {
      return (
          <div>
            <div>{this.state.data.cpu}</div>
            <div>{this.state.data.load}</div>
            <div>{this.state.data.processes}</div>
          </div>
      );
    }
});

var Content = React.createClass({
    render: function() {
        return (
            <div>
                <Counter />
                <br />
                <br />
                <br />
                <Counter />
            </div>
        );
    }
});

ReactDOM.render(
  <Content />,
  document.getElementById('main')
);

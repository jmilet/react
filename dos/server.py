#!/usr/bin/env python

import asyncio
import datetime
import random
import websockets
import subprocess
import json

async def time(websocket, path):
    while True:
        # now = datetime.datetime.utcnow().isoformat() + 'Z'
        now = str(subprocess.check_output("top -d 1", shell=True))
        now = now.split("\\n")

        processes = now[0].replace("b'", "")
        load = now[1].replace("b'", "")
        cpu = now[2].replace("b'", "")

        await websocket.send(json.dumps({
            "processes": processes,
            "load": load,
            "cpu": cpu
        }))
        await asyncio.sleep(random.random())

start_server = websockets.serve(time, '37.139.2.214', 8000)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
